import React from 'react';
import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider, StyledEngineProvider } from "@mui/material/styles";
//
import dark from "./assets/jss/themes/dark"
import App from './App';

import { createRoot } from 'react-dom/client';
const container = document.getElementById('root');
const root = createRoot(container); // createRoot(container!) if you use TypeScript

root.render(
  <StyledEngineProvider injectFirst>
    <ThemeProvider theme={dark}>
      <CssBaseline />
        <App />
    </ThemeProvider>
  </StyledEngineProvider>
);

const SmartRename = (stringName, newName, width, height, unit) => {
  let lastDot = stringName.lastIndexOf('.');

  if (lastDot < 0) {
    return newName;
  }

  return `${unit}_${width}_${height}_${newName}${stringName.slice(lastDot)}`;
}

const SmartRenameCSV = (stringName, newName) => {
  let lastDot = stringName.lastIndexOf('.');

  if (lastDot < 0) {
    return newName;
  }

  return `${newName}${stringName.slice(lastDot)}`;
}

export { SmartRename, SmartRenameCSV }
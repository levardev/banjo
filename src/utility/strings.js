const getFirstPart = (str, part) => {
  // find the index of the first underscore
  let index = str.lastIndexOf(part);
  // if there is no underscore, return the whole string
  if (index === -1) {
    return str;
  }
  // otherwise, return the substring from the start to the previous character
  return str.substring(0, index);
};

export default getFirstPart;
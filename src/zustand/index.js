import create from 'zustand'

export const useStore = create(() => {
  return {
    connected: false,
    notBusy: false,
    ip: "none",
    query: {},
    exportTime: false,
    hrx: {},
    hrxVersion: 0,
    hrxPayload: {},
    USDZIntent: "",
    GLBIntent: "",
    //
    name: "",
    usdz: {not: "loaded"},
    glb: {not: "loaded"},
    uploadTime: false,
    uploaded: false
  }
});
import React, { useEffect } from 'react';
import { useStore } from "./zustand";
import BuildPlaywright from './make';

const getQueryStringParams = () => {
  const url = new URL(window.location.href);
  const params = new URLSearchParams(url.search);
  const paramsObject = {};
  for (const [key, value] of params) {
    paramsObject[key] = value;
  };
  return paramsObject;
};

const App = () => {
  const handleQS = React.useCallback(async () => {
    const params = getQueryStringParams(window.location.search);
    useStore.setState({ query: params });
  }, []);

  useEffect(() => {
    handleQS();
  }, [handleQS]);

  return <BuildPlaywright />;
};

export default App;
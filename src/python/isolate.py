import cv2
import numpy as np
import secrets
import string
import subprocess

# Generate a random string
rand_string = ''.join(secrets.choice(string.ascii_lowercase + string.digits) for _ in range(8))

# Load the image
image = cv2.imread('lightningMask.jpg', cv2.IMREAD_GRAYSCALE)

# Threshold the image to binary
_, binary = cv2.threshold(image, 128, 255, cv2.THRESH_BINARY)

# Find contours in the binary image
contours, _ = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

for i, contour in enumerate(contours):
    # Check if contour has more than 3 points
    if len(contour) > 3:
        # Create a black image
        mask = np.zeros_like(image)
        
        # Draw the contour on the black image
        cv2.drawContours(mask, [contour], -1, (255), thickness=cv2.FILLED)
        
        # Compare with the original mask and correct negative enclaves
        mask = np.where((mask > 230) & (image < 10), 0, mask)
        
        # Save the image with random string included in the name
        mask_name = f'mask_{rand_string}_{i}.png'
        cv2.imwrite(mask_name, mask)

        # Call the outline4.py script
        subprocess.run(['python3', 'outline.py', mask_name])
    else:
        # If contour doesn't have more than 3 points, it's skipped.
        print(f"Contour at index {i} skipped because it has less than 4 points.")

subprocess.run(['python3', 'jsoncombine.py', rand_string])
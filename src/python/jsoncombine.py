import os
import glob
import json
import sys

# Check if argument was provided
if len(sys.argv) < 2:
    raise ValueError("Please provide the random string as an argument")

# Get the random string from the argument
random_string = sys.argv[1]

# The directory containing JSON files
directory = './'

# Find all JSON files in the directory
json_files = glob.glob(os.path.join(directory, f'*{random_string}*.json'))

total_data = []

# Loop through the json files
for json_file in json_files:
    # Open each file
    with open(json_file, 'r') as file:
        # Load the JSON data
        data = json.load(file)
        # Add the data from each file into the total list
        total_data.extend(data)

# Create the top-level structure with version, type and size
final_data = {
    "data": total_data,
    "version": 1,
    "type": "levar 2D mask",
    "size": [0, 5]
}

# Save all data into the final .l2v file
with open(f'contours_mask_{random_string}_final.json', 'w') as f:
    json.dump(final_data, f)
import cv2
import numpy as np
import json
import sys

# Check if argument was provided
if len(sys.argv) < 2:
    raise ValueError("Please provide the mask name as an argument")

# Get the mask name from the argument
mask_name = sys.argv[1]

# Load the image
img = cv2.imread(mask_name, cv2.IMREAD_GRAYSCALE)

# Threshold the image to binary
_, binary_img = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY)

# Find all contours in the binary image
contours, hierarchy = cv2.findContours(binary_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

# Create an empty black image
outline = np.zeros_like(img)

# Draw all the contours on the empty image
cv2.drawContours(outline, contours, -1, (255), 1)

# Save the result
cv2.imwrite(f'outline_{mask_name}', outline)

# Helper function to convert contour data to list of tuples
def contour_to_list(contour):
    return [(int(point[0][0]), int(point[0][1])) for point in contour]

# Creating a JSON object for contours
contour_json = []

for i in range(len(contours)):
    contour_dict = {}

    # Store contour data
    contour_dict['contour'] = contour_to_list(contours[i])

    # Check and store holes if any
    holes = []
    next_contour_index = hierarchy[0][i][2]
    while next_contour_index != -1:
        holes.append(contour_to_list(contours[next_contour_index]))
        next_contour_index = hierarchy[0][next_contour_index][0] # get 'next' index from hierarchy
   
    contour_dict['holes'] = holes
    contour_json.append(contour_dict)

# Iterate through each contour
for contour in contour_json:

    # Check if the contour has holes
    if len(contour['holes']) > 0:

        # Store all the contours in a list for comparison
        all_contour_data = [c['contour'] for c in contour_json]

        # Iterate through the holes
        for hole in contour['holes']:
            
            # Check if the hole matches with any other contour
            if hole in all_contour_data:
                # If it matches, remove the matching contour
                matching_contour = [c for c in contour_json if c['contour'] == hole]
                if matching_contour:
                    contour_json.remove(matching_contour[0])

# Save the contour data to a JSON file
with open(f'contours_{mask_name}.json', 'w') as file:
    json.dump(contour_json, file)
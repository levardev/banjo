import { useState, useEffect } from 'react';
import * as AWS from "@aws-sdk/client-s3";
import { useStore } from '../../../zustand';
import { useHistory } from "react-router-dom";

const { REACT_APP_ACCESS_ID, REACT_APP_ACCESS_KEY } = process.env;

const REACT_APP_GHOST_BUCKET = "levar-ghosts-models"

const s3client = new AWS.S3({
  region: "us-east-1",
  credentials: {
    accessKeyId: REACT_APP_ACCESS_ID,
    secretAccessKey: REACT_APP_ACCESS_KEY
  }
});

export const HaileyUpload = () => {
  const params = useStore.getState().query

  const history = useHistory();

  const usdz = useStore(state => state.usdz);
  const glb = useStore(state => state.glb);
  const name = useStore(state => state.name);

  const [justOnce, setJustOnce] = useState(false);

  useEffect(() => {
    if (!justOnce) {
      setJustOnce(true)

      const delay = (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
      };

      const getDirKey = (str, params) => {
        let dirKey = `ghosts/${params.store_id}/${params.bundle}/${str}`;
        return dirKey;
      };

      const extractString = (str) => {
        const parts = str.split('_');
        return parts[0];
      };

      const cleanName = extractString(name);

      const upload = async (blobUSDZ, blobGLB, blobName) => {
        const glbDirKey = `${getDirKey(blobName, params)}.glb`;
        const usdzDirKey = `${getDirKey(blobName, params)}.usdz`; // TEEHEE
        const glbUploadParams = { Bucket: REACT_APP_GHOST_BUCKET, Key: glbDirKey, Body: blobGLB };
        const usdzUploadParams = { Bucket: REACT_APP_GHOST_BUCKET, Key: usdzDirKey, Body: blobUSDZ };
        const glbCommand = new AWS.PutObjectCommand(glbUploadParams);
        const usdzCommand = new AWS.PutObjectCommand(usdzUploadParams);
        await s3client.send(glbCommand);
        await s3client.send(usdzCommand);
        await delay(100);
        useStore.setState({uploaded: true })
      };

      upload(usdz, glb, cleanName);
    }
  }, [usdz, glb, name, justOnce, params, history]);

  return (null);
};
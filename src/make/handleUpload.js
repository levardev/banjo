import React from 'react';
import { HaileyUpload } from './upload';
import { useStore } from '../../../zustand';

export default function HandleUpload() {
  const uploadTime = useStore(state => state.uploadTime);
  return (
    <>
      {uploadTime && <HaileyUpload />}
    </>
  );
};
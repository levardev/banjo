import React from "react";
import { Typography, Box, Container, Stack } from '@mui/material';
import THREED from "./THREED";
import { useStore } from "../zustand";

const BuildPlaywright = () => {
  const uploaded = useStore(state => state.uploaded)

  return (
    <Container
      maxWidth="sm"
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: '100vh',
        minWidth: '100vw',
        bgcolor: 'background.default',
        py: 4,
      }}>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          p: 2,
          borderRadius: 2,
          boxShadow: 3,
          bgcolor: '#222222',
        }}>
        <Stack spacing={1}>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              p: 2,
            }}>
            <Typography variant="h4">Generating AI 3D Mesh</Typography>
          </Box>

          <Box sx={{ width: "50vw", height: "50vh" }}>
            <THREED />
          </Box>
          {uploaded && <div id="TEEHEE">
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                p: 2,
              }}>
              <span title={"uploaded"}>true</span>
            </Box>
          </div>}
        </Stack>
      </Box>
    </Container>
  );
};

export default BuildPlaywright;
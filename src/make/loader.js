import { useEffect, useRef, useState } from 'react';
import * as THREE from 'three';
import dataStuff from "./contours.json"
import { useBounds } from '@react-three/drei';
import { useLoader } from '@react-three/fiber';
import { TextureLoader, NearestMipmapLinearFilter, LinearFilter, SRGBColorSpace } from "three";
import bcpic from "../python/jordan_color.jpg"

function generateUVsForExtrudeGeometry(geometry, textureWidth, textureHeight) {
  const uvs = [];
  
  const positionAttribute = geometry.attributes.position;

  console.log("Triangle Count", positionAttribute.count);
  
  for (let i = 0; i < positionAttribute.count; i++) {
    const x = positionAttribute.getX(i);
    const y = positionAttribute.getY(i);
    
    let u = x / textureWidth;
    let v = 1 - (y / textureHeight);

    uvs.push(u, v);
  }

  geometry.setAttribute('uv', new THREE.Float32BufferAttribute(uvs, 2));
}


const Loader = () => {
  const bounds = useBounds()

  const geoRef = useRef()
  const rotateGroupRef = useRef()
  const positionGroupRef = useRef()

  const [partTwo, setPartTwo] = useState(false);
  const [partOne, setPartOne] = useState(true);

  const BaseColor = () => {
    let color_map = useLoader(TextureLoader, bcpic);
    color_map = {
      ...color_map,
      colorSpace: SRGBColorSpace, anisotropy: 8, minFilter: NearestMipmapLinearFilter, magFilter: LinearFilter, name: "updated_levar_map"
    };

    return <texture attach="map" {...color_map} />
  };

  let fullLength = dataStuff.data.length;

  let geometries = [];

  dataStuff.data.forEach(contourData => {
    // Create an instance of THREE.Shape for the contour
    let shape = new THREE.Shape();

    // Move to the start point of the contour
    let startPoint = contourData.contour[0];
    shape.moveTo(startPoint[0], startPoint[1]);

    // Line to the other points of the contour
    for (let i = 1; i < contourData.contour.length; i++) {
      let point = contourData.contour[i];
      shape.lineTo(point[0], point[1]);
    }

    // For each hole, create an instance of THREE.Path and add it to the shape's holes
    contourData.holes.forEach(hole => {
      let path = new THREE.Path();

      // Move to the start point of the hole
      let holeStartPoint = hole[0];
      path.moveTo(holeStartPoint[0], holeStartPoint[1]);

      // Line to the other points of the hole
      for (let i = 1; i < hole.length; i++) {
        let holePoint = hole[i];
        path.lineTo(holePoint[0], holePoint[1]);
      }

      // Add the path to shape's holes
      shape.holes.push(path);
    });


    let extrudeSettings = {
      steps: 1,
      depth: 20, // This is the amount of extrusion along the z-axis.
      bevelEnabled: false
    };

    // Create geometry from the shape
    let geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);

    generateUVsForExtrudeGeometry(geometry, 2048, 2048);

    geometries.push(geometry)
  })

  useEffect(() => {
    if (rotateGroupRef.current.children.length === fullLength && partOne) {
      setPartOne(false)
      rotateGroupRef.current.rotation.z = Math.PI
      setPartTwo(true)
    }
  }, [partOne, rotateGroupRef, fullLength])

  useEffect(() => {
    if (partTwo) {
      setPartTwo(false)
      // Assuming you have your top group defined as 'group'
      var bbox = new THREE.Box3().setFromObject(positionGroupRef.current);

      // This will give you the size of the bounding box of the group
      var center = bbox.getCenter(new THREE.Vector3());

      positionGroupRef.current.position.x = -center.x;
      positionGroupRef.current.position.y = -center.y;

      bounds.refresh().fit();
    }
  }, [partTwo, bounds, positionGroupRef])

  return (
    <>
      <group ref={positionGroupRef}>
        <group ref={rotateGroupRef}>
          <mesh geometry={geometries[0]} ref={geoRef}>
            <meshStandardMaterial attach="material" side={THREE.DoubleSide} >
              <BaseColor />
            </meshStandardMaterial>
          </mesh>
          {geometries.map((prop, key) => {
            if (key > 0) {
              return (
                <mesh geometry={prop} key={key}>
                  <meshStandardMaterial attach="material" side={THREE.DoubleSide} >
                    <BaseColor />
                  </meshStandardMaterial>
                </mesh>
              )
            } else return null
          })}
        </group>
      </group>
    </>
  );
}

export default Loader
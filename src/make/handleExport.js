import React from 'react';
import { HaileyExport } from './export';
import { useStore } from '../../../zustand';

export default function HandleExport() {
  const exportTime = useStore(state => state.exportTime);
  return (
    <>
      {exportTime && <HaileyExport/>}
    </>
  );
};
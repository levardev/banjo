import React from 'react';
import { Canvas } from '@react-three/fiber';
import ShadeScene from './scene';

export default function THREED() {
  return (
    <Canvas
      camera={{ position: [0, 0, -4000], fov: 54, near: 1, far: 50000 }}
      dpr={1}
    >
      <ShadeScene />
    </Canvas>
  );
};
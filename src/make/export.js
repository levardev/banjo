import { useState, useEffect } from 'react';
import { useThree } from '@react-three/fiber'
// import { GLTFExporter } from "../../../utility/elm_glb";
import { GLTFExporter } from 'three/examples/jsm/exporters/GLTFExporter'
import { USDZExporter } from "../../../utility/oak_usdz";
import { useStore } from '../../../zustand';

export const HaileyExport = () => {
  const tscene = useThree(state => state.scene)

  const params = useStore.getState().query

  const [usdzDone, setUSDZDone] = useState(false);
  const [glbDone, setGLBDone] = useState(false);

  useEffect(() => {
    const exportGLB = async (input, name) => {
      const exporter = new GLTFExporter();

      const options = {
        trs: false,
        onlyVisible: false,
        truncateDrawRange: false,
        binary: true,
        maxTextureSize: "2048",
        forceIndices: false
      };

      exporter.parse(input, async function (gltf) {
        const blobber = new Blob([gltf], { type: 'application/octet-stream' });
        useStore.setState({glb: blobber})
        setGLBDone(true)
      }, function (error) {
        console.log('An error happened', error);
      }, options);
    };

    const exportUSDZ = async (input) => {
      const exporter = new USDZExporter("horizontal");

      const arraybuffer = await exporter.parse(input);
      const blob = new Blob([arraybuffer], { type: 'application/octet-stream' });
      useStore.setState({usdz: blob})
      setUSDZDone(true)
    };

    const hero = tscene.getObjectByName("DeliveryTarget");

    if (params.name && hero) {
      console.log("inside")
      exportUSDZ(hero);
      exportGLB(hero);
      useStore.setState ({name: params.name})
    }
  }, [tscene, params.name]);

  useEffect(() => {
    if (glbDone && usdzDone) {
      useStore.setState({uploadTime: true})
    };
  }, [usdzDone, glbDone])

  return (null);
};
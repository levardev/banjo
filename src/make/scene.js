import React, { Suspense } from 'react';
import { OrbitControls } from '@react-three/drei';
import Loader from './loader';
import { Bounds } from '@react-three/drei';


const ShadeScene = () => {
  return (
    <React.Fragment>
      <ambientLight />
      <OrbitControls />
      <color attach="background" args={['white']} />
      <Suspense fallback={null}>
        <Bounds>
          <group>
            <Loader />
          </group>
        </Bounds>
      </Suspense>
    </React.Fragment>
  );
}

export default ShadeScene;
